<?php
/*
 * 应用中心主页：http://addon.discuz.com/?@ailab
 * 人工智能实验室：Discuz!应用中心十大优秀开发者！
 * 插件定制 联系QQ594941227
 * From www.ailab.cn
 */
 
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

require_once DISCUZ_ROOT.'./source/plugin/jsonapi/functions.php';
jsonApiLoad();
$op=$_GET['op'];
if($op=='add'){
	$style=$_GET['style'];
	if(submitcheck('addsubmit')){
		$data=array();
		$data['appname']=addslashes(trim($_POST['appname']));
		$data['appkey']=addslashes(trim($_POST['appkey']));
		if(!$data['appkey']) $data['appkey']=strtolower(random(10));
		foreach($_POST['apilist'] as $k=>$v){
			if($v==0){
				unset($_POST['apilist'][$k]);
			}
		}
		$data['apilist']=serialize($_POST['apilist']);
		if(!$data['appname']){
			cpmsg('对不起，必须填写应用名称！');
			exit;
		}
		DB::insert('jsonapi_applist',$data,true);
		cpmsg('应用授权添加成功！','action=plugins&operation=config&do='.$pluginid.'&identifier=jsonapi&pmod=applist', 'succeed');

	}else{
		showformheader("plugins&operation=config&do=$pluginid&identifier=jsonapi&pmod=applist&op=add");
		showtableheader('新增接口授权', 'nobottom');		
		showsetting('应用名称','appname','','text','', 0,'输入要授权的应用名称');
		showsetting('appkey','appkey','','text','', 0,'由英文和字母组成的随机字符串，留空随机产生');
		foreach($_JSONAPI['class'] as $k=>$C){
			showsetting($C::$_api.' 接口','apilist['.$C::$_api.']','','radio','', 0,'选择是否授权');
		}
		showsubmit('addsubmit');
		showtablefooter();
		showformfooter();
	}	
}elseif($op=='edit'){
	$appid=intval($_GET['appid']);
	if(submitcheck('editsubmit')){
		$data=array();
		$data['appname']=addslashes(trim($_POST['appname']));
		$data['appkey']=addslashes(trim($_POST['appkey']));
		if(!$data['appkey']) $data['appkey']=strtolower(random(10));
		foreach($_POST['apilist'] as $k=>$v){
			if($v==0){
				unset($_POST['apilist'][$k]);
			}
		}
		$data['apilist']=serialize($_POST['apilist']);
		if(!$data['appname']){
			cpmsg('对不起，必须填写应用名称！');
			exit;
		}
		DB::update('jsonapi_applist',$data,array('appid'=>$appid));
		cpmsg('接口授权编辑成功！','action=plugins&operation=config&do='.$pluginid.'&identifier=jsonapi&pmod=applist', 'succeed');
	}else{
		$info=DB::fetch_first("select * from ".DB::table('jsonapi_applist')." where appid='$appid'");
		$apilist=unserialize($info['apilist']);
		showformheader("plugins&operation=config&do=$pluginid&identifier=jsonapi&pmod=applist&op=edit&appid=$appid");
		showtableheader('编辑接口授权', 'nobottom');		
		showsetting('应用名称','appname',$info['appname'],'text','', 0,'输入要授权的应用名称');
		showsetting('appkey','appkey',$info['appkey'],'text','', 0,'由英文和字母组成的随机字符串，留空随机产生');
		foreach($_JSONAPI['class'] as $k=>$C){
			showsetting($C::$_api.' 接口','apilist['.$C::$_api.']',$apilist[$C::$_api]? 1:0,'radio','', 0,'选择是否授权');
		}
		showsubmit('editsubmit');
		showtablefooter();
		showformfooter();
	}
}else{//列表
	//默认列表;
	showtableheader('接口授权管理列表[<a href="'.ADMINSCRIPT.'?action=plugins&operation=config&do='.$pluginid.'&identifier=jsonapi&pmod=applist&op=add&style=code"><font color="red">增加授权</font></a>]', 'nobottom');
	showsubtitle(array('appid','appkey','应用名称','授权接口','编辑'));
	$pagenum=50;
	$count=DB::result_first("select count(*) from ".DB::table('jsonapi_applist')." ");
	$rules=DB::fetch_all("select * from ".DB::table('jsonapi_applist')." order by appid desc");
	foreach($rules as $adid=>$rule){
		$apilist=array_keys(unserialize($rule['apilist']));
		showtablerow('',array(), array(
			$rule['appid'],
			$rule['appkey'],
			$rule['appname'],
			implode(',',$apilist),
			'<a href="###" onclick="location.href=\''.ADMINSCRIPT.'?action=plugins&operation=config&do='.$pluginid.'&identifier=jsonapi&pmod=applist&op=edit&appid='.$rule['appid'].'\'">编辑</a>',
		));
	}
	showtablefooter();
	echo multi($count,$pagenum,$page,ADMINSCRIPT."?action=plugins&operation=config&do=$pluginid&identifier=jsonapi&pmod=applist");

}
?>