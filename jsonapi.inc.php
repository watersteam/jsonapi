<?php
/*
 * 应用中心主页：http://addon.discuz.com/?@ailab
 * 人工智能实验室：Discuz!应用中心十大优秀开发者！
 * 插件定制 联系QQ594941227
 * From www.ailab.cn
 */
 
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//plugin.php?id=jsonapi&token=test&api=threadlist&s_fid=2&s_subject=1&f_subject=like&orderby=tid&sort=desc
require_once DISCUZ_ROOT.'./source/plugin/jsonapi/functions.php';
loadcache('plugin');
$vars=$_G['cache']['plugin']['jsonapi'];
$pagenum=intval($vars['pagenum']);
$token=trim($_GET['token']);
$appid=intval($_GET['appid']);
if((!$token&&!$appid)||($token&&$appid)){
	showApiError(-100);
}
if($token&&$token!=$vars['token']){
	showApiError(-99);
}
$api=trim($_GET['api']);
if($appid){
	//检查appid权限问题
	$appkey=trim($_GET['appkey']);
	if(!$appkey){
		showApiError(-97);
	}
	$app=DB::fetch_first("select * from ".DB::table('jsonapi_applist')." where appid='$appid'");
	if(!$app){
		showApiError(-96);
	}
	if($app['appkey']!=$appkey){
		showApiError(-95);
	}
	$apilist=unserialize($app['apilist']);
	if(!$apilist[$api]){
		showApiError(-94);
	}
}
jsonApiLoad();
$apiclass='jsonapi_'.$api;
if(in_array($apiclass,$_JSONAPI['class'])){
	$_class=new $apiclass;
	//api开启 条件
	if(!$_class->_config['status']){
		showApiError(-93);
	}
	//return 条件
	if(!$_class->_config['return']){
		showApiError(-89);
	}
	
	//limit 条件
	$page=max(1,intval($_GET['page']));
	$maxpage=intval($vars['maxpage']);
	if($maxpage&&$page>$maxpage){
		showApiError(-88);
	}
	if($_class->_pagenum){
		$pagenum=$_class->_pagenum;
		
	}
	$limit=" limit ".($pagenum*($page-1)).",$pagenum";
	
	//where 条件 筛选字段
	$where='';
	foreach($_class->_config['filter'] as $f=>$v){
		if($_GET['s_'.$f]){
			if($_GET['f_'.$f]=='like'){
				$where.=" and $f like '%".addslashes(trim($_GET['s_'.$f]))."%'";
			}elseif($_JSONAPI['wherefilter'][$_GET['f_'.$f]]){
				$where.=" and $f ".$_JSONAPI['wherefilter'][$_GET['f_'.$f]]." '".addslashes(trim($_GET['s_'.$f]))."'";
			}else{
				$where.=" and $f = '".addslashes(trim($_GET['s_'.$f]))."'";
			}
		}
	}
	
	//order by 条件 排序字段
	$orderby=addslashes(trim($_GET['orderby']));
	if($orderby&&$_class->_config['orderby'][$orderby]) $orderby=" order by $orderby";
	else $orderby='';
	$sort=addslashes(trim($_GET['sort']));
	if($sort&&!in_array($sort,$_JSONAPI['sort'])){
		$sort='';
	}
	if(!$orderby) $sort='';

	//从接口中获取数据
	$data=$_class->getData($where,$orderby,$sort,$limit);
	DB::insert('jsonapi_apilogs',array(
		'appid'=>$appid,
		'api'=>$api,
		'status'=>100,
		'dateline'=>TIMESTAMP,
	));
	
	//返回json数据 转为utf-8编码
	echo json_encode(array(
		'status'=>100,
		'charset'=>'utf-8',
		'page'=>$page,
		'pagenum'=>$pagenum,
		'allcount'=>$data['allcount'],
		'data'=>arrayToUtf8($data['data']),
	));
	exit;
}else{
	showApiError(-98);
}

?>